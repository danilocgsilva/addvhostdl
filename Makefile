BIN ?= addvhostdl
PREFIX ?= /usr/local

install:
	cp addvhostdl.sh $(PREFIX)/bin/$(BIN)
	chmod +x $(PREFIX)/bin/$(BIN)

uninstall:
	rm -f $(PREFIX)/bin/$(BIN)
