#!/bin/bash

## version
VERSION="0.0.0"

## Checks if the current execution can write to the files to make the installation
can_write_files () {
  if [ ! -w /etc/hosts ]
  then
    echo I can \'t write in the /etc/hosts files. Checks if it was granted the right permission. You can try rexecute the commando with sudo.
    exit
  fi
}

## Asks for hostname
asks_for_host_name () {
  read -p "Provides the virtual host entry, the address that you will access through the browser: " host_name
  echo $host_name
}

## Tries to create the host
tries_to_create_host_folder () {
  if ! [ $(mkdir -p $1 2> /dev/null) ]
  then
    echo It was not possible to create the site folder.
    echo Verifies if you have sufficient permission to create the folder.
    echo You can try run with sudo, or can point to a place that already exists.
    exit
  fi
}

## Validates site path. If not exists, tries to create one. Otherwise, halts the script with error.
validates_site_path () {
  checks_first_character $1
  if [ ! -d $1 ]
  then
    tries_to_create_host_folder $1
  fi
}

## Checks first character
checks_first_character () {
  if [ ${1:0:1} != / ]
  then
    echo For site path, you must provides an absolute path, starting with /.
    exit
  fi
}

## Asks for site path
asks_for_site_path () {
  read -p "Provides the full path of your site: " site_path
  echo $site_path
}

## Main function
addvhostdl () {
  can_write_files
  host_name=$(asks_for_host_name)
  site_path=$(asks_for_site_path)
  validates_site_path $site_path

  echo The host name is $host_name
  echo The real site path is $site_path
}

## detect if being sourced and
## export if so else execute
## main function with args
if [[ /usr/local/bin/shellutil != /usr/local/bin/shellutil ]]; then
  export -f addvhostdl
else
  addvhostdl "${@}"
  exit 0
fi
