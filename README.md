# addvhostdl - Add Virtual Host to debian like system

First, you can install this utility in your system. Just navigate to the folder project an run:

```
make install
```

Then, you can run the script anywhere from your system to create a new virtual host in your development machine.

This scripts:

1. Adds a new entry in the /etc/hosts, so address from your browser will hit the own machine.
2. Adds basic configurations to the new virtual host.
